import { ReactNode } from "react"

type Props ={
  children: ReactNode,
  title: string
}

export default function FooterInfo({ title, children }: Props) {
  return (
    <div className='flex flex-col'>
      <h1 className='font-bold text-[18px] mb-2'>
        {title}
      </h1>
      {children}
    </div>
  )
}