import Container from "src/Components/Container";
import FooterInfo from "./FooterInfo";
import Image from "next/image";
import tkp from 'public/assets/home/tkp.webp'


export default function Footer() {
  return (
    <div className='mt-48'>
      <Container>
        <div className='flex flex-1 justify-start gap-[80px]'>
          <FooterInfo title='Tersedia di'>
            <div className='grid'>
              <div className='w-[200px] h-[63px] mt-3 mb-6'>
                <Image
                  src={tkp}
                />
              </div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.4855800377422!2d106.89846899999999!3d-6.331076299999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ed18514f3077%3A0x32145046e0f3216b!2sSignum%20Coffee!5e0!3m2!1sid!2sid!4v1634477699035!5m2!1sid!2sid" width="400" height="300" loading="lazy"></iframe>
            </div>

          </FooterInfo>
          <FooterInfo title='Buka Setiap'>
            <div className='flex flex-1 gap-7'>
              <div className='font-normal text-base'>
                <p>Minggu - Selasa</p>
                <p>11.00 - 21.00</p>
              </div>
              <div className='font-normal text-base'>
                <p>Rabu - Sabtu</p>
                <p>11.00 - 22.00</p>
              </div>
            </div>
          </FooterInfo>
          <div className='grid'>
            <FooterInfo title='Alamat'>
              <div className='font-normal text-base mb-5'>
                <p>Jl. Cilangkap Raya No.37, RT.9/RW.3,<br /> Cipayung, Kec. Cipayung, Kota Jakarta Timur,<br /> Daerah Khusus Ibukota Jakarta 13840</p>
              </div>
            </FooterInfo>
            <div className='mt-[-118px]'>
              <FooterInfo title='Nomor Telepon'>
                <div className='font-normal text-base mb-5'>
                  <p>+6287788443553</p>
                </div>
              </FooterInfo>
            </div>
          </div>
        </div>
      </Container>
    </div>
  )
}