import Image from "next/image"
import Container from "src/Components/Container"
import Carousel from "../Carousel"
import style from './styles.module.css'
import 'react-multi-carousel/lib/styles.css'

export default function TopSection() {
  return (
    <div className='w-auto'>
      <section className={`${style.background} text-white`}>
        <Container>
          <div id='top'>
            <h1 className='font-medium text-7xl pt-[97vh]'>Selamat Datang di <br /> Signum Coffee</h1>
            <p className='text-[18px] mt-3'>Beli aneka produk di Toko Signum Coffee secara online sekarang. Kamu bisa beli produk <br /> dari Toko Signum Coffee dengan aman & mudah dari Cipayung, Kota Administrasi Jakarta <br /> Timur. Nikmati juga fitur Cicilan 0% sehingga kamu bisa belanja online dengan nyaman di <br /> Tokopedia. </p>
            <button className='h-[45px] w-[177px] bg-[#0D6971] rounded-3xl text-[16px] font-medium mt-6'>Beli Sekarang</button>
          </div>
          <div id='menu'>
            <div className='text-center mt-14'>
              <h1 className='font-medium text-4xl'>Menu</h1>
              <div className='mt-8 justify-center'>
                <Carousel />
              </div>
            </div>
          </div>
        </Container>
      </section>
    </div>
  )
}