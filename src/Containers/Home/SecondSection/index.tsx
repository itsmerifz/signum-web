import Container from "src/Components/Container"
import style from './styles.module.css'
import Image from "next/image"
import showup from 'public/assets/home/showup.svg'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faInstagram } from "@fortawesome/free-brands-svg-icons"
import Footer from "../Footer"

export default function SecondSection() {
  return (
    <div className='w-auto'>
      <section className={`${style.background} text-white`}>
        <Container>
          <h1 className='text-center font-medium text-4xl pt-[222px]'>Social Media</h1>
          <div className='flex flex-auto justify-center gap-12'>
            <div className='h-[427px] w-[549px] mt-[60px] ml-6'>
              <Image
                src={showup}
              />
            </div>
            <div className='mt-[120px]'>
              <h1 className={`font-medium text-7xl ${style.txt}`}>Temukan <br /> Beragam Suasana</h1>
              <div className='flex mt-5'>
                <i className='text-[32px]'><FontAwesomeIcon
                icon={faInstagram} size='2x' /></i>
                <h2 className='text-[32px] font-normal ml-4 mt-2'> @signum.coffee</h2>
              </div>
            </div>
          </div>
          <Footer/>
        </Container>
      </section>
    </div>
  )
}