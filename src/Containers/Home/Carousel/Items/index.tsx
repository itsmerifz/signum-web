import Image from "next/image";
import gradient from 'public/assets/home/gradient.svg'

export default function Items({ image, label, harga }) {
  return (
    <div className='bg-[#c4c4c4] h-[339px] w-[245px] mx-auto rounded-2xl'>
      <div className={`h-[339px] w-[245px] relative`}>
        <div className='absolute z-50 bottom-[63px]'>
          <Image
            src={gradient}
          />
        </div>
        <div className='absolute z-50 bottom-[25px] left-3 text-black text-left'>
          <h1 className='font-light text-[16px]'>{label}</h1>
          <p className='font-bold text-[24px]'>Rp. {harga}</p>
        </div>
        <Image
          src={image}
        />
      </div>
    </div>
  )
}