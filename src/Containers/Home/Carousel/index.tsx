import Carousel from "react-grid-carousel"
import Items from "./Items"
import bg1 from 'public/assets/home/1.svg'
import bg2 from 'public/assets/home/2.svg'
import bg3 from 'public/assets/home/3.svg'
import bg4 from 'public/assets/home/4.svg'
import bg5 from 'public/assets/home/5.svg'
import bg6 from 'public/assets/home/6.svg'

const dotStyle = ({ isActive }) => (
  <span
    style={{
      display: 'inline-block',
      height: isActive ? '8px' : '5px',
      borderRadius: isActive ? '24px' : '50%',
      width: isActive ? '28px' : '5px',
      background: isActive ? '#775' : '#fff',
      transitionTimingFunction: 'ease-in-out',
      transitionDuration: '300ms',
      transitionProperty: 'all',
      marginTop: '50px'
    }}></span>
)

export default function Carousels() {
  return (
    <Carousel cols={3}
      rows={1}
      gap={20}
      loop
      showDots
      dot={dotStyle}
    >
      <Carousel.Item>
        <Items image={bg1}
          label='Red Velvet Latte 500ml'
          harga='35.000' />
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg2}
          label='Dark Chocolate Latte 500ml'
          harga='35.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg3}
          label='Kopi Tentara Terpesona 250ml & Krakakoa Blinkies'
          harga='42.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg4}
          label='Matchalicious 500ml'
          harga='35.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg5}
          label='KISS Kopi Susu & Blinkies (Signum Coffee x Krakakoa)'
          harga='69.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg6}
          label='Kopi Susu Gula Aren Caramel 500ml'
          harga='40.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg1}
          label='Red Velvet Latte 500ml'
          harga='35.000' />
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg2}
          label='Dark Chocolate Latte 500ml'
          harga='35.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg3}
          label='Kopi Tentara Terpesona 250ml & Krakakoa Blinkies'
          harga='42.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg4}
          label='Matchalicious 500ml'
          harga='35.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg5}
          label='KISS Kopi Susu & Blinkies (Signum Coffee x Krakakoa)'
          harga='69.000'/>
      </Carousel.Item>
      <Carousel.Item>
        <Items image={bg6}
          label='Kopi Susu Gula Aren Caramel 500ml'
          harga='40.000'/>
      </Carousel.Item>
    </Carousel>
  )
}