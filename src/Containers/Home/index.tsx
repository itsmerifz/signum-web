import Container from "src/Components/Container";
import Navbar from "src/Components/Navbar";
import SecondSection from "./SecondSection";
import TopSection from "./TopSection";

export default function Home() {
  return (
    <div className='bg-[#051719] overflow-x-hidden'>
      <Navbar />
      <TopSection />
      <SecondSection />
    </div>
  )
}