import { ReactNode } from "react";

type Props = {
  children: ReactNode,
  size?: string,
  className?: string
}

export default function Container({ children, size = 'xl',className=''}: Props){
  return(
    <div className={`${size === 'xl' ? 'max-w-screen-xl' : 'max-w-screen-lg'} mx-auto ${className}`}>
      {children}
    </div>
  )
}