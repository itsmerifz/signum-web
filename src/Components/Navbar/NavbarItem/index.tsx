import Link from 'next/link'

type Props = {
  label: string,
  href : string
}

export default function NavbarItem({ label, href = '/'}){
  return(
    <Link href={href}>
      <a className='text-white font-semibold uppercase'>{label}</a>
    </Link>
  )
}