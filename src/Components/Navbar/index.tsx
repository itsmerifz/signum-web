import Container from "../Container";
import NavbarItem from "./NavbarItem";
import Image from 'next/image';
import logoNav from '../../../public/assets/icons/horizon-logo.png';
import { useState, useEffect } from "react";

export default function Navbar() {
  const [show, setShow] = useState(false)
  const [logo, setLogo] = useState(false)

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 100) {
        setShow(true)
        setLogo(true)
      } else {
        setShow(false)
        setLogo(false)
      }
    })
    return () => {
      window.removeEventListener('scroll', null)
    }
  }, [])

  return (
    <header className={`h-[70px] ${show && 'bg-[#061819]'} p-4 z-20 fixed w-full duration-700 ease-in transition-all`}>
      <Container>
        <nav className='flex'>
          <div className='flex-1'>
            <div className={`transition-all duration-700 ease-in-out w-[80px] ${logo ? 'opacity-100' : 'opacity-0'}`}>
              {logo &&
                <Image
                  src={logoNav}
                  alt='logo'
                />}
            </div>
          </div>
          <div className='flex justify-end items-center gap-8 mb-2'>
            <NavbarItem label='Home' />
            <NavbarItem label='Menu' />
            <NavbarItem label='Social Media' />
            <NavbarItem label='About' />
          </div>
        </nav>
      </Container>
    </header>
  )
}